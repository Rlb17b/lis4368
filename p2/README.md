# LIS4381 - Advanced Web Applications Development

## Richard L. Bong, Information Technology Undergraduate 

### Project 2 Requirements 

#### Deliverables:

    - Valid user form entry (customerform.jsp)
    - Passed Validation (thanks.jsp)
    - Display Data (customers.jsp)
    - Modify Form (modify.jsp)
    - Modified Data (customers.jsp)
    - Delete Warning (customers.jsp)



**README.md file should include the following items:**
*Screenshot of ERD that links to the image:*

    This a blockquote.

    This is the second paragraph in the blockquote.

#### Project Screenshot and Links:

*Screenshot P2 Valid User Form Entry* :
![P2 Screenshot1] (img/p2pic1.png)
*Screenshot P2 Passed Validation* :
![P2 Screenshot2] (img/p2pic2.png)
*Screenshot P2 Display Data* :
![P2 Screenshot3] (img/p2pic3.png)
*Screenshot P2 Modify Form* :
![P2 Screenshot4] (img/p2pic4.png)
*Screenshot P2 Modified Data* :
![P2 Screenshot5] (img/p2pic5.png)
*Screenshot P2 Delete Warning* :
![P2 Screenshot6] (img/p2pic6.png)
*Screenshot P2 Database CRUD* :
![P2 Screenshot6] (img/p2pic7.png)



