**NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted
# LIS4381 - Advanced Web Applications Development

## Richard L. Bong, Information Technology Undergraduate 

### Assignment 1 Requirements: 

*Includes:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations 
3. Chapter Questions (Chs 1, 2)
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation [My PHP Installation](http://localhost:9999 "lis4368");
* Screenshot of running java Hello;
* git commands w/short descriptions;
* Bitbucket repo links a) this assignment and b) the completed tutorial above (bitbucketstationlocations).

This is a blockquote.

This is the second paragraph in the blockquote.

#### Git commands w/short descriptions:

git init - Create a new local repository 
git status - List the files you've changed and those you still need to add or commit
git add - Add one or more files to staging (index):
git commit - Records changes to the repository
git push - Sends changes to the master branch of your remote repository 
git pull - Fetch and merge changes on the remote server to your working directory
git clone - Create a working copy of a local repository

#### Assignment Screenshots 

*Screenshot of running java Hello*:

![JDK  Installation Screenshot] (img/jdk_install.png)

*Screenshot of Tomcat running http://localhost:9999

![Tomcat running] (img/tomcat.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Location:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mjowett/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/mjowett/myteamquotes/ "My Team Quotes Tutorial")



