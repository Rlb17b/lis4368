# LIS4381 - Advanced Web Applications Development

## Richard L. Bong, Information Technology Undergraduate 

### Assignment 3 Requirements 

#### Deliverables:

1. Entity Relationship Diagram (ERD)
2. Include data (at lease 10 records each table)
3. Provide Bitbucket read-only access to repo (Language SQL) **must** include README.md, using Markdown syntax, and include links to all of the following files (from README.md);
* docs folder: a3.mwb, and a3.sql
* img folder: a3.png (export a3.mwb file as a3.png)
* README.md (**MUST** display a3.png ERD)

**README.md file should include the following items:**
*Screenshot of ERD that links to the image:*

    This a blockquote.

    This is the second paragraph in the blockquote.

#### Assignment Screenshot and Links:

*Screenshot A3 ERD* :

![A3 ERD] (img/a3.png)

*A3 docs: a3.mwb and a3.sql

[A3 MWB File](docs/a3.mwb)

[A3 SQL File](docs/a3.sql)