# LIS4381 - Advanced Web Applications Development

## Richard L. Bong, Information Technology Undergraduate 

### Project 1 Requirements 

#### Deliverables:
1. Provide Bitbucketread-only access to lis4368repo, includelinks to the other assignment repos you created in README.md, using Markdownsyntax(README.mdmust also include screenshots as per above.)
2. Blackboard Links:lis4368Bitbucketrepo3.*Note*:the carousel *must*contain (min. 3) slides that YOUcreated, that contain text andimages that link to other content areasmarketing/promoting your skills.

**README.md file should include the following items:**
*Screenshots of input validation testing*

    This a blockquote.

    This is the second paragraph in the blockquote.

#### Project Screenshots and Links:

*Screenshot P1 input validation* :

Screenshot 1           |  Screenshot 2
:-------------------------:|:-------------------------:
![P1 Input Validation 1] (img/p1.png)  |  ![P1 Input Validation 2] (img/p11.png)




