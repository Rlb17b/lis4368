package crud.data;

import java.sql.*;
import javax.sql.DataSource;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ConnectionPool
{
    private static ConnectionPool pool = null;
    private static DataSource dataSource = null;

    private ConnectionPool()
    {
        try
        {
            InitialContext ic = new InitialContext();
            dataSource = (DataSource) ic.lookup("java:/comp/env/jdbc/rlb17b");
        }
        catch (NamingException e)
        {
            System.out.println(e);
        }
    }

    public static synchronized ConnectionPool getInstance()
    {
        if (pool == null)
        {
            pool = new ConnectionPool();
        }
        return pool;
    }

    public Connection getConnection()
    {
        try
        {
            return dataSource.getConnection();
        }
        catch (SQLException e)
        {
            System.out.println(e);
            return null;
        }
    }

    public void freeConnection(Connection c)
    {
        try
        {
            c.close();
        }
        catch (SQLException e)
        {
            System.out.println(e);
        }
    }

}





/*
Notes:
Connection Pooling:
Opening/Closing database connections is resource intensive.
Connection pools improve DB connection performance--as well as the associated execution of commands on a database.
Facilitates reuse of same connection object--rather than opening/closing many connections--to serve a number of client requests.

The Java Naming and Directory Interface (JNDI) is a Java API for a directory service
that allows Java software clients to discover and look up data and objects via a name.
https://en.wikipedia.org/wiki/Java_Naming
*/