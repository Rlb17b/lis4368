# LIS4381 - Advanced Web Applications Development

## Richard L. Bong, Information Technology Undergraduate 

### Assignment 2 Requirements: 

*Includes:*

1. MySQLClient Login Using AMPPS:
2. Finish tutorial
3. How to Debug?(This is an important section of the tutorial—refer back to it!)
4. Assessment links

#### README.md file should include the following items:

* Assessment links
* Only *one* screenshot of the query results from the following link [Querybook](http://localhost:9999/hello/querybook.html "lis4368"); 

#### Assessment links

* [hello link](http://localhost:9999/hello "hello") (displays directory, needs index.html)
* [hellohome link](http://localhost:9999/hello/index.html "index") (Rename"HelloHome.html" to "index.html")
* [sayhello link](http://localhost:9999/hello/sayhello "sayhello") (invokes HelloServlet) Note: /sayhello maps to HelloServlet.class(changed web.xml file)
* [querybook link](http://localhost:9999/hello/querybook.html "querybook")
* [sayhi link](http://localhost:9999/hello/sayhi "sayhi") (invokes AnotherHelloServlet)

#### Assignment Screenshots 

*Screenshot of http://localhost:9999/hello/query?author=Tan+Ah+Teck

![localhost querybook] (img/TanAhTeck.png)

