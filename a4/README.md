# LIS4381 - Advanced Web Applications Development

## Richard L. Bong, Information Technology Undergraduate 

### Assignment 4 Requirements 

#### Deliverables:

1. Server-side Validation
2. Compiling Servlet Files

**README.md file should include the following items:**
*Screenshot of ERD that links to the image:*

    This a blockquote.

    This is the second paragraph in the blockquote.

#### Assignment Screenshot and Links:

*Screenshot A4 Server-side Validation* :


Screenshot 1           |  Screenshot 2
:-------------------------:|:-------------------------:
![A4 Failed Validation] (img/a4pic.png)  |  ![A4 Passed Validation] (img/a4pic1.png)

