> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4368 Advanced Web Applications Development 

## Richard L. Bong

### LIS 4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install jdk
    - Install Tomcat

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - MySQL running in AMPPS
    - Compiling Servlet Files

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create 3 ERD's using MYSQL
    - USe proper relationships between entities 
    - Forward engineer to root connection
    - Export MWB and SQL files

4. [P1 README.md](p1/README.md "My p1 README.md file")
    - Bootstrap client-side validation
    - META-INF and WEB-INF
    - Modifying meta tags
    - Adding form controls
    - Using min/max jQuery validation
    - Using regexp to only allow appropriate characters for each control
    - Using HTML5 property to limit the number of characters for each control
    - Valid, invalid, validation code 

5. [A4 README.md](a4/README.md "My a4 README.md file")
    - Server-side Validation
    - Modifying and debugging servlet files
    - Compiling Servlet Files

6. [A5 README.md](a5/README.md "My a5 README.md file")
    - Server-side Validation
    - Compiling Servlet Files
    - Pre and post valid user form entry
    - MySQL customer table entry 

7. [P2 README.md](p2/README.md "My p2 README.md file")
    - Provide create, read, update, and delete functionality 
    - pre/post valid user entry forms
    - MySQL customer table entry
    - JSTL to prevent XSS