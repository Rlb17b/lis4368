# LIS4381 - Advanced Web Applications Development

## Richard L. Bong, Information Technology Undergraduate 

### Assignment 5 Requirements 

#### Deliverables:

1. Server-side Validation
2. Compiling Servlet Files
3. Pre and post valid user form entry
4. MySQL customer table entry 


**README.md file should include the following items:**
*Screenshot of ERD that links to the image:*

    This a blockquote.

    This is the second paragraph in the blockquote.

#### Assignment Screenshot and Links:

*Screenshot A5 Server-side Validation* :


Screenshot 1           |  Screenshot 2
:-------------------------:|:-------------------------:
![A5 Valid User Form Entry] (img/a5pic1.png)  |  ![A5 Passed Validation (thanks.jsp)] (img/a5pic2.png)

![A5 Associated Database Entry] (img/a5pic3.png)
